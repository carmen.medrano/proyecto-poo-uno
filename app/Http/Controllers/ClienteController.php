<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function InicioCliente(Request $request)
    {

        $cliente = Cliente::all();
      return view('clientes.inicio')-> with ('cliente', $cliente);
    }

    public function CrearCliente(Request $request)
    {
        $cliente = Cliente::all();
        return view('clientes.crear')-> with ('cliente', $cliente);
    }

    public function GuardarCliente(Request $request)
    {
        $this ->validate($request,[
          'Nombre' => 'required',
          'Apellidos' => 'required',
          'Cedula' => 'required',
          'Direccion' => 'required',
          'Telefono' => 'required',
          'Fecha_nacimiento' => 'required',
          'Email' => 'required'
        ]);
    
    $producto = new Cliente;
    $producto ->Nombre = $request-> Nombre;
    $producto ->Apellidos = $request-> Apellidos;
    $producto ->Cedula = $request-> Cedula;
    $producto ->Direccion = $request-> Direccion;
    $producto ->Telefono = $request-> Telefono;
    $producto ->Fecha_nacimiento = $request-> Fecha_nacimiento;
    $producto ->Email = $request-> Email;
    $producto ->save();
    return redirect()->route('list.clientes');
    }
}
