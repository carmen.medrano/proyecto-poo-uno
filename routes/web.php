Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home','HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){

    Route::post('/guardar/productos',['as'=>'guardar.productos','uses' => 'ProductoController@GuardarProducto']);
    Route::get('/lista/productos',['as'=>'list.productos','uses' => 'ProductoController@InicioProducto']);
    Route::get('/crear/productos',['as'=>'crear.productos','uses' => 'ProductoController@CrearProducto']);
    Route::get('/lista/clientes',['as'=>'list.clientes','uses' => 'ClienteController@InicioCliente']);
    Route::get('/crear/clientes',['as'=>'crear.clientes.clientes','uses' => 'ClienteController@CrearCliente']);
    Route::post('/guardar/clientes',['as'=>'guardar.clientes.clientes','uses' => 'ClienteController@GuardarCliente']);

});

