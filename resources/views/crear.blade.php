@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR CLIENTES</div>

                <div class="col text-right">
                  <a href="{{ route('list.clientes') }}" class="btn btn-sm btn-success">Cancelar</a>
                </div>

                <div class="card-body">
                    <form role="form" method="post" action="{{ route('guardar.cliente')}}">
                    {{ csrf_field() }}
                    {{method_field('post')}}
                    
                    <div class="row">
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Nombre">Nombre</label>
                        <input type="text" class="from-control" name="Nombre">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Apellidos">Apellidos</label>
                        <input type="text" class="from-control" name="Apellidos">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Cedula">Cedula</label>
                        <input type="text" class="from-control" name="Cedula">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Direccion">Direccion</label>
                        <input type="text" class="from-control" name="Direccion">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Telefono">Telefono</label>
                        <input type="text" class="from-control" name="Telefono">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Fecha_nacimiento">Fecha_nacimiento</label>
                        <input type="text" class="from-control" name="Fecha_nacimiento">
                    </div>
                        <div class="col-lg-4">
                        <label class="from-control-label" for="Email">Email</label>
                        <input type="text" class="from-control" name="Email">
                    </div>

                    </div>
                    <button type="submit" class="btn btn-success pull-rigth"> Guardar </button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection